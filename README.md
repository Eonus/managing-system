### Steps to start the program

0. **Required**
   1. Node.js
   2. npm
1. `git clone https://gitlab.com/Eonus/managing-system.git`
2. `cd managing-system`
3. `npm i`
4. `npm run build`
5. **Create .env** file in root folder with such fields:
   1. `MONGODB_STRING` (https://docs.mongodb.com/manual/reference/connection-string/)
   2. `DB_NAME`
   3. `PORT` (server will starts using this port)
6. `npm run test` or `npm run coverage`
7. `npm run start`

[endpoints docs](endpoints.md#anchortext)
