All responses has property status - `ok` or `error`. All responses has `uuid` field too. It is unique request and response identifier, which used to be able to see info about this request/response later.
If response has status `error`, then it has property `error` too, with property `message` inside it.
Examples:
`{status:"ok", uuid:"uuid"}`
`{status:"error", error:{message:"Such was not found"}, uuid:"uuid"}`
further, the request and response parameters are indicated without the above and for the case of successful execution

---

### Endpoints

### /product

**POST**

```
request:{ name:string, price:number }
response:{ objectId:string }
```

**GET**

```
request:{ }
response:{
	elements:Array<{
		uuid:string,
		name:string,
		price:number,
		creationTime:number
	}>
}
```

---

### /product/`uuid`

**GET**

```
request:{ }
response:{
	element:{
		uuid:string,
		name:string,
		price:number,
		creationTime:number
	}
}
```

**UPDATE**

```
request:{
	name?:string,
	price?:number,
	creationTime?:number
}
response:{}
```

**DELETE**

```
request:{}
response:{}
```

### /order

---

**POST**

```
request:{ productId:string }
response:{ objectId:string }
```

**GET**

```
request:{ }
response:{
	elements:Array<{
		uuid:string,
		productId:string,
		product:{
			uuid:string,
			name:string,
			price:number
		}
		status:string, // "created" or "processed" or "completed"
		creationTime:number
	}>
}
```

---

### /order/`uuid`

**GET**

```
request:{ }
response:{
	element:{
		uuid:string,
		productId:string,
		product:{
			uuid:string,
			name:string,
			price:number
		}
		status:string, // "created" or "processed" or "completed"
		creationTime:number
	}
}
```

**UPDATE**

```
request:{
	productId?:string,
	status?:string, // "created" or "processed" or "completed"
	creationTime:?number
}
response:{}
```

**DELETE**

```
request:{}
response:{}
```

---

### /invoice

**POST**

```
request:{ orderId:string }
response:{ objectId:string }
```

**GET**

```
request:{ }
response:{
	elements:Array<{
		uuid:string,
		orderId:string,
		order:{
			productId:string,
			product:{
				uuid:string,
				name:string,
				price:number
			},
			creationTime:number
		}
		discount:number, // between 0 and 100
		finalPrice:number
	}>
}
```

### /invoice/`uuid`

**GET**

```
request:{ }
response:
	element:{
		uuid:string,
		orderId:string,
		order:{
			productId:string,
			product:{
				uuid:string,
				name:string,
				price:number
			},
			creationTime:number
		}
		discount:number, // between 0 and 100
		finalPrice:number
	}
}
```

**UPDATE**

```
request:{
	orderId?:string,
	discount?:number // between 0 and 100
}
response:{}
```

**DELETE**

```
request:{}
response:{}
```
