import { BaseStorageObjectInterface } from "."

export enum FilterValues {
  More = "$more",
  Less = "$less",
}

export type OneKeyFilter<Value> = {
  [key in FilterValues]?: Value
}

export type Filter<ObjectInterface extends BaseStorageObjectInterface> = {
  [Key in keyof ObjectInterface]?: OneKeyFilter<ObjectInterface[Key]>
}
