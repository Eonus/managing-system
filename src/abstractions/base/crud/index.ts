import { Filter } from "./filter"

export interface BaseStorageObjectInterface {
  uuid: string
}

export interface StorageInterface<
  ObjectInterface extends BaseStorageObjectInterface
> {
  create: (object: ObjectInterface) => Promise<void>
  read: (uuid: string) => Promise<ObjectInterface>
  update: (uuid: string, updateInfo: Partial<ObjectInterface>) => Promise<void>
  delete: (uuid: string) => Promise<void>
  readMany: (filter?: Filter<ObjectInterface>) => Promise<ObjectInterface[]>
}
