import { InitializerInterface } from "."

export interface EnvInitializerInterface<EnvSchema, EnvType>
  extends InitializerInterface {
  initialize: (schema: EnvSchema) => void
  getEnv: () => EnvType
}
