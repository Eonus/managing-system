export interface InitializerInterface {
  initialize?: (...params: any) => any
  connect?: (...params: any) => any
  disconnect?: (...params: any) => any
}
