export enum PrimitiveType {
  String = "string",
  Number = "number",
  Boolean = "boolean",
  Any = "any",
}

export type Primitive = {
  name: string
  type: PrimitiveType
  canBeUndefined?: boolean
  canBeNull?: boolean
  validate?: (value: any) => void
}

export type ComplexParseScheme = {
  name?: string
  primitives: Primitive[]
  nested?: (ComplexParseScheme & { name: string; canBeUndefined?: boolean })[]
}

export type ParseScheme = Primitive | ComplexParseScheme
