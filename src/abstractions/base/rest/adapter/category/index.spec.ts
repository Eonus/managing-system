export interface RestAdapterTesterInterface {
  getManySuccess: (...params: any) => Promise<any>
  getManyError?: (...params: any) => Promise<any>
  createOneSuccess: (...params: any) => Promise<any>
  createOneError?: (...params: any) => Promise<any>
  updateOneSuccess: (...params: any) => Promise<any>
  updateOneError?: (...params: any) => Promise<any>
  getOneSuccess: (...params: any) => Promise<any>
  getOneError?: (...params: any) => Promise<any>
  deleteOneSuccess: (...params: any) => Promise<any>
  deleteOneError?: (...params: any) => Promise<any>
}
