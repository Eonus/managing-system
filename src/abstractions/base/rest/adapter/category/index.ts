export enum RestApiMethod {
  Post,
  Get,
  Update,
  Delete,
  GetAll,
}

export interface RestApiCategoryAdapterInterface {
  call: (method: RestApiMethod, data: any, uuid?: string) => Promise<any>
}
