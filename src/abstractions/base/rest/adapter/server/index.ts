import { RestApiMethod } from "../category"

export interface RestApiServerAdaperInterface {
  count: (
    category: string,
    method: RestApiMethod,
    data: any,
    uuid?: string
  ) => Promise<any>
  isCategoryExist: (category: string) => boolean
}
