import {
  BaseRestObjectInterface,
  RestApiHandlerInterface,
} from "../base/rest/handler"
import { InvoiceInterface } from "."

export interface InvoiceRestObject
  extends BaseRestObjectInterface,
    Partial<InvoiceInterface> {
  uuid: string
  orderId: string
  discount?: number
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InvoiceRestApiHandlerInterface
  extends RestApiHandlerInterface<InvoiceRestObject, InvoiceInterface> {}
// интерфейс остается интерфейсом, несмотря на то, что он пустой, чтобы потом его можно было имплементировать
