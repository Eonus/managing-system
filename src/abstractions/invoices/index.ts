export interface InvoiceProductInterface {
  uuid: string
  name: string
  price: number
}

export interface InvoiceOrderInterface {
  productId: string
  product: InvoiceProductInterface
  creationTime: number
}

export interface InvoiceInterface {
  uuid: string
  orderId: string
  order: InvoiceOrderInterface
  creationTime: number
  discount: number // percentage
  finalPrice: number
}
