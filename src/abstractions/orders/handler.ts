import { OrderInterface, OrderStatus } from "."
import {
  BaseRestObjectInterface,
  RestApiHandlerInterface,
} from "../base/rest/handler"

export interface OrderRestObject
  extends BaseRestObjectInterface,
    Partial<OrderInterface> {
  uuid: string
  productId: string
  status?: OrderStatus
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface OrderRestApiHandlerInterface
  extends RestApiHandlerInterface<OrderRestObject, OrderInterface> {}
// интерфейс остается интерфейсом, несмотря на то, что он пустой, чтобы потом его можно было имплементировать
