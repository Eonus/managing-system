export interface OrderProductInterface {
  uuid: string
  name: string
  price: number
}

export enum OrderStatus {
  Created = "created",
  Processed = "processed",
  Completed = "completed",
}

export interface OrderInterface {
  uuid: string
  productId: string
  product: OrderProductInterface
  status: OrderStatus
  creationTime: number
}
