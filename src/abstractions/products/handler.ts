import { ProductInterface } from "."
import {
  BaseRestObjectInterface,
  RestApiHandlerInterface,
} from "../base/rest/handler"

export interface ProductRestObject
  extends BaseRestObjectInterface,
    Partial<ProductInterface> {
  uuid: string
  name: string
  price: number
  creationTime?: number
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ProductRestApiHandlerInterface
  extends RestApiHandlerInterface<ProductRestObject, ProductInterface> {}
// интерфейс остается интерфейсом, несмотря на то, что он пустой, чтобы потом его можно было имплементировать
