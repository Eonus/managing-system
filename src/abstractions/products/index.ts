export interface ProductInterface {
  uuid: string
  name: string
  price: number
  creationTime: number
}
