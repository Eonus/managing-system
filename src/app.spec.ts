/* eslint-env mocha */
import { expect } from "chai"
import { EnvInitializer } from "./realisations/initializers/env"
import { MongooseInitializer } from "./realisations/initializers/mongoose"

describe("app.ts", () => {
  let env: { MONGODB_STRING: string; DB_NAME: string } | undefined
  describe("enviroment initializer", () => {
    it("initialize", () => {
      const envInitializer = new EnvInitializer()
      expect(() =>
        envInitializer.initialize({
          MONGODB_STRING: String,
          DB_NAME: String,
        }),
      ).not.to.throw()
    })
    it("getEnv", () => {
      const envInitializer = new EnvInitializer<{
        MONGODB_STRING: string
        DB_NAME: string
      }>()
      envInitializer.initialize({
        MONGODB_STRING: String,
        DB_NAME: String,
      })
      expect(() => {
        env = envInitializer.getEnv()
      }).not.to.throw()
    })
  })

  describe("mongoose initializer", () => {
    const mongooseInitializer = new MongooseInitializer()
    it("connect", async () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //@ts-ignore
      await mongooseInitializer.connect(env?.MONGODB_STRING, env?.DB_NAME)
    })
    it("disconnect", async () => {
      await mongooseInitializer.disconnect()
    })
  })
})
