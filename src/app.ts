import { EnvInitializer } from "./realisations/initializers/env"
import { MongooseInitializer } from "./realisations/initializers/mongoose"
import { InvoiceStorage } from "./realisations/invoices/crud/typegoose"
import { InvoiceRestApiHandler } from "./realisations/invoices/handler"
import { OrderStorage } from "./realisations/orders/crud/typegoose"
import { OrderRestApiHandler } from "./realisations/orders/handler"
import { ProductStorage } from "./realisations/products/crud/typegoose"
import { ProductRestApiHandler } from "./realisations/products/handler"
import { RestApiCategoryAdapterFactory } from "./realisations/rest/adapter/category/factory"
import { RestApiServerAdaper } from "./realisations/rest/adapter/server"
import { RestExpressServer } from "./realisations/server/express/rest"

const envInitializer = new EnvInitializer<{
  MONGODB_STRING: string
  DB_NAME: string
  PORT: number
}>()
envInitializer.initialize({
  MONGODB_STRING: String,
  DB_NAME: String,
  PORT: Number,
})
const env = envInitializer.getEnv()
const mongooseInitializer = new MongooseInitializer()
mongooseInitializer.connect(env.MONGODB_STRING, env.DB_NAME)

const productStorage = new ProductStorage()
const productHandler = new ProductRestApiHandler(productStorage)

const orderStorage = new OrderStorage()
const orderHandler = new OrderRestApiHandler(orderStorage, productStorage)

const invoiceStorage = new InvoiceStorage()
const invoiceHandler = new InvoiceRestApiHandler(
  invoiceStorage,
  productStorage,
  orderStorage,
)

const server = new RestExpressServer(
  new RestApiServerAdaper(
    new RestApiCategoryAdapterFactory(
      productHandler,
      orderHandler,
      invoiceHandler,
    ),
  ),
)
server.initialize()
server.connect(env.PORT)
