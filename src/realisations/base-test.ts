/* eslint-env mocha */
import { EnvInitializer } from "./initializers/env"
import { MongooseInitializer } from "./initializers/mongoose"
import * as sinon from "sinon"

export const useBase = (): {
  MONGODB_STRING: string
  DB_NAME: string
  PORT: number
} => {
  let mongooseInitializer: MongooseInitializer
  let sandbox: sinon.SinonSandbox
  const envInitializer = new EnvInitializer<{
    MONGODB_STRING: string
    DB_NAME: string
    PORT: number
  }>()
  envInitializer.initialize({
    MONGODB_STRING: String,
    DB_NAME: String,
    PORT: Number,
  })
  const env = envInitializer.getEnv()
  before(async () => {
    sandbox = sinon.createSandbox()
    mongooseInitializer = new MongooseInitializer()
    const stub = sandbox.stub(console, "log")
    await mongooseInitializer.connect(env.MONGODB_STRING, env.DB_NAME)
    stub.restore()
  })

  after(async () => {
    const stub = sandbox.stub(console, "log")
    await mongooseInitializer.disconnect()
    stub.restore()
    sandbox.restore()
  })

  return env
}
