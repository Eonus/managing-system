import { StorageInterface } from "../../../abstractions/base/crud"
import { Filter, FilterValues } from "../../../abstractions/base/crud/filter"
import { BaseRestObjectInterface } from "../../../abstractions/base/rest/handler"

export abstract class TypegooseStorageAbstract<
  ObjectInterface extends BaseRestObjectInterface
> implements StorageInterface<ObjectInterface> {
  abstract create: (object: ObjectInterface) => Promise<void>
  abstract read: (uuid: string) => Promise<ObjectInterface>
  abstract update: (
    uuid: string,
    updateInfo: Partial<ObjectInterface>
  ) => Promise<void>

  abstract delete: (uuid: string) => Promise<void>
  abstract readMany: (
    filter?: Filter<ObjectInterface>
  ) => Promise<ObjectInterface[]>

  public convertFilter = (filter?: Filter<ObjectInterface>): any => {
    if (!filter) return {}
    const result: any = {}
    for (const key in filter) {
      for (const filterValue in filter[key]) {
        switch (filterValue) {
          case FilterValues.More: {
            if (!result[key]) result[key] = { $gt: filter[key][filterValue] }
            else result[key].$gt = filter[key][filterValue]
            break
          }
          case FilterValues.Less: {
            if (!result[key]) result[key] = { $lt: filter[key][filterValue] }
            else result[key].$lt = filter[key][filterValue]
            break
          }
        }
      }
    }

    return result
  }
}
