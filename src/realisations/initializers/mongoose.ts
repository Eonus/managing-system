import { MongooseInitializerInterface } from "../../abstractions/base/initializer/mongoose"
import * as mongoose from "mongoose"

export class MongooseInitializer implements MongooseInitializerInterface {
  connect = async (mongoDbString: string, dbName: string): Promise<void> => {
    try {
      await mongoose.connect(mongoDbString, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        dbName,
      })
      console.log("Mongoose connected success")
    } catch (e) {
      console.error("Mongoose connect error " + e.message)
    }
  }

  disconnect = async (): Promise<void> => {
    try {
      await mongoose.disconnect()
      console.log("Mongoose disconnected success")
    } catch (e) {
      console.error("Mongoose disconnect error " + e.message)
    }
  }
}
