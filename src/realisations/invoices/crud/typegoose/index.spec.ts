/* eslint-env mocha */
import { expect } from "chai"
import { InvoiceStorage } from "."
import { useBase } from "../../../base-test"

describe("invoice", () => {
  describe("crud", () => {
    describe("typegoose", () => {
      useBase()
      const invoiceStorage = new InvoiceStorage()
      const uuid = "test uuid"
      const obj = {
        uuid,
        orderId: "order id",
        order: {
          productId: "product id",
          product: {
            uuid: "product uuid",
            name: "product name",
            price: 20,
          },
          creationTime: Date.now(),
        },
        creationTime: Date.now(),
        discount: 20,
        finalPrice: 16,
      }
      it("create", async () => {
        await invoiceStorage.create(obj)
      })
      it("create exist", async () => {
        try {
          await invoiceStorage.create(obj)
        } catch (e) {
          return
        }
        throw Error("Was created")
      })
      it("read exist", async () => {
        const product = await invoiceStorage.read(uuid)
        expect(product).to.property("uuid", uuid)
      })
      it("update", async () => {
        await invoiceStorage.update(uuid, {
          discount: 25,
          finalPrice: 15,
        })
        const invoice = await invoiceStorage.read(uuid)
        expect(invoice).to.property("discount", 25)
        expect(invoice).to.property("finalPrice", 15)
      })
      it("readMany", async () => {
        await invoiceStorage.readMany()
      })
      it("delete exist", async () => {
        await invoiceStorage.delete(uuid)
        try {
          await invoiceStorage.read(uuid)
        } catch (e) {
          return
        }
        throw Error("Was not deleted")
      })
      it("read unexist", async () => {
        try {
          await invoiceStorage.read(uuid)
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was readed")
      })
      it("update unexist", async () => {
        try {
          await invoiceStorage.update(uuid, {})
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was updated")
      })
      it("delete unexist", async () => {
        try {
          await invoiceStorage.delete(uuid)
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was readed")
      })
    })
  })
})
