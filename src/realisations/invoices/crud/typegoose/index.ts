import { Filter } from "../../../../abstractions/base/crud/filter"
import { InvoiceInterface } from "../../../../abstractions/invoices"
import { TypegooseStorageAbstract } from "../../../crud/typegoose"
import { InvoiceModel } from "./model"

export class InvoiceStorage extends TypegooseStorageAbstract<InvoiceInterface> {
  create = async (object: InvoiceInterface): Promise<void> => {
    await InvoiceModel.create(object)
  }

  read = async (uuid: string): Promise<InvoiceInterface> => {
    const result = await InvoiceModel.findOne({ uuid })
    if (!result) throw Error("Such was not found")
    return result
  }

  update = async (
    uuid: string,
    updateInfo: Partial<InvoiceInterface>,
  ): Promise<void> => {
    const result = await InvoiceModel.findOneAndUpdate({ uuid }, updateInfo)
    if (!result) throw Error("Such was not found")
  }

  delete = async (uuid: string): Promise<void> => {
    const result = await InvoiceModel.findOneAndDelete({ uuid })
    if (!result) throw Error("Such was not found")
  }

  readMany = async (
    filter?: Filter<InvoiceInterface>,
  ): Promise<InvoiceInterface[]> => {
    return await InvoiceModel.find(this.convertFilter(filter))
  }
}
