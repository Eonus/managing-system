import { getModelForClass, prop } from "@typegoose/typegoose"
import {
  InvoiceInterface,
  InvoiceOrderInterface,
  InvoiceProductInterface,
} from "../../../../abstractions/invoices"

class InvoiceProduct implements InvoiceProductInterface {
  @prop()
  uuid!: string

  @prop()
  name!: string

  @prop()
  price!: number
}

class InvoiceOrder implements InvoiceOrderInterface {
  @prop()
  productId!: string

  @prop()
  product!: InvoiceProduct

  @prop()
  creationTime!: number
}

class Invoice implements InvoiceInterface {
  @prop({ unique: true })
  uuid!: string

  @prop()
  orderId!: string

  @prop()
  order!: InvoiceOrder

  @prop()
  creationTime!: number

  @prop()
  discount!: number

  @prop()
  finalPrice!: number
}
export const InvoiceModel = getModelForClass(Invoice)
