import { StorageInterface } from "../../../abstractions/base/crud"
import { InvoiceInterface } from "../../../abstractions/invoices"
import {
  InvoiceRestApiHandlerInterface,
  InvoiceRestObject,
} from "../../../abstractions/invoices/handler"
import { v4 as uuid } from "uuid"
import { ProductInterface } from "../../../abstractions/products"
import { OrderInterface } from "../../../abstractions/orders"
import { Filter } from "../../../abstractions/base/crud/filter"
import * as moment from "moment"

export class InvoiceRestApiHandler implements InvoiceRestApiHandlerInterface {
  private storage: StorageInterface<InvoiceInterface>
  private productStorage: StorageInterface<ProductInterface>
  private orderStorage: StorageInterface<OrderInterface>
  constructor(
    invoiceStorage: StorageInterface<InvoiceInterface>,
    productStorage: StorageInterface<ProductInterface>,
    orderStorage: StorageInterface<OrderInterface>,
  ) {
    this.storage = invoiceStorage
    this.productStorage = productStorage
    this.orderStorage = orderStorage
  }

  getOne = async (uuid: string): Promise<InvoiceInterface> => {
    return await this.storage.read(uuid)
  }

  getMany = async (
    filter?: Filter<InvoiceInterface>,
  ): Promise<InvoiceInterface[]> => {
    return await this.storage.readMany(filter)
  }

  post = async (object: Omit<InvoiceRestObject, "uuid">): Promise<string> => {
    // TODO: add discount counting
    const objectOrder = await this.orderStorage.read(object.orderId)
    const objectProduct = await this.productStorage.read(objectOrder.productId)
    const discount = this.countDiscount(objectProduct)
    const fullObject: InvoiceInterface = {
      uuid: uuid(),
      ...object,
      creationTime: Date.now(),
      order: {
        productId: objectOrder.productId,
        product: {
          uuid: objectProduct.uuid,
          name: objectProduct.name,
          price: objectProduct.price,
        },
        creationTime: objectOrder.creationTime,
      },
      discount,
      finalPrice: (1 - discount / 100) * objectProduct.price,
    }
    await this.storage.create(fullObject)
    return fullObject.uuid
  }

  put = async (
    uuid: string,
    object: Partial<InvoiceRestObject>,
  ): Promise<void> => {
    if (object.orderId) {
      const objectOrder = await this.orderStorage.read(object.orderId)
      const objectProduct = await this.productStorage.read(
        objectOrder.productId,
      )
      const discount = object.discount || this.countDiscount(objectProduct)
      object = {
        ...object,
        order: {
          productId: objectOrder.productId,
          product: {
            uuid: objectProduct.uuid,
            name: objectProduct.name,
            price: objectProduct.price,
          },
          creationTime: objectOrder.creationTime,
        },
        discount,
        finalPrice: (1 - discount / 100) * objectProduct.price,
      }
    } else if (object.discount) {
      const objectData = await this.getOne(uuid)
      object = {
        ...object,
        discount: object.discount,
        finalPrice:
          (1 - object.discount / 100) * objectData.order.product.price,
      }
    }
    await this.storage.update(uuid, object)
  }

  delete = async (uuid: string): Promise<void> => {
    await this.storage.delete(uuid)
  }

  private countDiscount = (objectProduct: ProductInterface): number => {
    const monthAgoMoment = moment().subtract(1, "month")
    if (monthAgoMoment.valueOf() > objectProduct.creationTime) return 20
    return 0
  }
}
