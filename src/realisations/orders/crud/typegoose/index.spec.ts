/* eslint-env mocha */
import { expect } from "chai"
import { OrderStorage } from "."
import { OrderStatus } from "../../../../abstractions/orders"
import { useBase } from "../../../base-test"

describe("order", () => {
  describe("crud", () => {
    describe("typegoose", () => {
      useBase()
      const orderStorage = new OrderStorage()
      const uuid = "test uuid"
      it("create", async () => {
        await orderStorage.create({
          uuid,
          creationTime: Date.now(),
          productId: "213",
          product: {
            uuid: "product uuid",
            name: "product name",
            price: 2320,
          },
          status: OrderStatus.Created,
        })
      })
      it("create exist", async () => {
        try {
          await orderStorage.create({
            uuid,
            creationTime: Date.now(),
            productId: "213",
            product: {
              uuid: "product uuid",
              name: "product name",
              price: 2320,
            },
            status: OrderStatus.Created,
          })
        } catch (e) {
          return
        }
        throw Error("Was created")
      })
      it("read exist", async () => {
        const product = await orderStorage.read(uuid)
        expect(product).to.property("uuid", uuid)
      })
      it("update", async () => {
        await orderStorage.update(uuid, {
          product: {
            name: "product name 2",
            uuid: "product uuid",
            price: 2320,
          },
        })
        const product = (await orderStorage.read(uuid)).product
        expect(product).to.property("name", "product name 2")
      })
      it("readMany", async () => {
        await orderStorage.readMany()
      })
      it("delete exist", async () => {
        await orderStorage.delete(uuid)
        try {
          await orderStorage.read(uuid)
        } catch (e) {
          return
        }
        throw Error("Was not deleted")
      })
      it("read unexist", async () => {
        try {
          await orderStorage.read(uuid)
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was readed")
      })
      it("update unexist", async () => {
        try {
          await orderStorage.update(uuid, {})
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was updated")
      })
      it("delete unexist", async () => {
        try {
          await orderStorage.delete(uuid)
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was readed")
      })
    })
  })
})
