import { Filter } from "../../../../abstractions/base/crud/filter"
import { OrderInterface } from "../../../../abstractions/orders"
import { TypegooseStorageAbstract } from "../../../crud/typegoose"
import { OrderModel } from "./model"

export class OrderStorage extends TypegooseStorageAbstract<OrderInterface> {
  create = async (object: OrderInterface): Promise<void> => {
    await OrderModel.create(object)
  }

  read = async (uuid: string): Promise<OrderInterface> => {
    const result = await OrderModel.findOne({ uuid })
    if (!result) throw Error("Such was not found")
    return result
  }

  update = async (
    uuid: string,
    updateInfo: Partial<OrderInterface>,
  ): Promise<void> => {
    const result = await OrderModel.findOneAndUpdate({ uuid }, updateInfo)
    if (!result) throw Error("Such was not found")
  }

  delete = async (uuid: string): Promise<void> => {
    const result = await OrderModel.findOneAndDelete({ uuid })
    if (!result) throw Error("Such was not found")
  }

  readMany = async (
    filter?: Filter<OrderInterface>,
  ): Promise<OrderInterface[]> => {
    return await OrderModel.find(this.convertFilter(filter))
  }
}
