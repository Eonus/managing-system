import { getModelForClass, prop } from "@typegoose/typegoose"
import {
  OrderInterface,
  OrderProductInterface,
  OrderStatus,
} from "../../../../abstractions/orders"

class OrderProduct implements OrderProductInterface {
  @prop()
  uuid!: string

  @prop()
  name!: string

  @prop()
  price!: number
}

class Order implements OrderInterface {
  @prop({ unique: true })
  uuid!: string

  @prop()
  productId!: string

  @prop()
  product!: OrderProduct

  @prop()
  status!: OrderStatus

  @prop()
  creationTime!: number
}
export const OrderModel = getModelForClass(Order)
