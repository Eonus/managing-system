import { StorageInterface } from "../../../abstractions/base/crud"
import { OrderInterface, OrderStatus } from "../../../abstractions/orders"
import {
  OrderRestApiHandlerInterface,
  OrderRestObject,
} from "../../../abstractions/orders/handler"
import { v4 as uuid } from "uuid"
import { ProductInterface } from "../../../abstractions/products"

export class OrderRestApiHandler implements OrderRestApiHandlerInterface {
  private storage: StorageInterface<OrderInterface>
  private productStorage: StorageInterface<ProductInterface>
  constructor(
    orderStorage: StorageInterface<OrderInterface>,
    productStorage: StorageInterface<ProductInterface>,
  ) {
    this.storage = orderStorage
    this.productStorage = productStorage
  }

  getOne = async (uuid: string): Promise<OrderInterface> => {
    return await this.storage.read(uuid)
  }

  getMany = async (): Promise<OrderInterface[]> => {
    return await this.storage.readMany()
  }

  post = async (object: Omit<OrderRestObject, "uuid">): Promise<string> => {
    const fullObject: OrderInterface = {
      uuid: uuid(),
      ...object,
      product: await this.productStorage.read(object.productId),
      status: object.status || OrderStatus.Created,
      creationTime: Date.now(),
    }
    await this.storage.create(fullObject)
    return fullObject.uuid
  }

  put = async (
    uuid: string,
    object: Partial<OrderRestObject>,
  ): Promise<void> => {
    if (object.productId) {
      object.product = await this.productStorage.read(object.productId)
    }
    await this.storage.update(uuid, object)
  }

  delete = async (uuid: string): Promise<void> => {
    await this.storage.delete(uuid)
  }
}
