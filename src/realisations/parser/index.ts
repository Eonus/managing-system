import {
  Primitive,
  ParseScheme,
  ComplexParseScheme,
} from "../../abstractions/base/parser"

export const parseData = <T>(parseScheme: ParseScheme, data: unknown): T => {
  if ((<Primitive>parseScheme).type) {
    return getParam(parseScheme as Primitive, data) as T
  }
  parseScheme = parseScheme as ComplexParseScheme
  const result: any = {}
  for (let i = 0; i < parseScheme.primitives.length; i++) {
    const value = getParam(parseScheme.primitives[i], data)
    if (value !== undefined) result[parseScheme.primitives[i].name] = value
  }
  if (!parseScheme.nested) return result as T
  const dataObj = <{ [key: string]: any }>data
  for (let i = 0; i < parseScheme.nested?.length; i++) {
    const nestedName = parseScheme.nested[i].name
    if (!data) {
      throw Error("No " + nestedName + "(object) sended")
    }
    if (
      typeof dataObj[nestedName] === "undefined" ||
      dataObj[nestedName] === null
    ) {
      if (dataObj[nestedName] === null) {
        throw Error("False type with " + nestedName + " - null instead object")
      }
      if (!parseScheme.nested[i].canBeUndefined) {
        throw Error("No " + nestedName + "(object) sended")
      }
    } else {
      const value = parseData(
        parseScheme.nested[i],
        (<{ [key: string]: any }>data)[nestedName],
      )
      result[parseScheme.nested[i].name] = value
    }
  }
  return result as T
}

const getParam = (info: Primitive, data: any): unknown => {
  if (!data) {
    throw Error("No " + info.name + "(" + info.type + ") sended")
  }
  if (typeof data[info.name] === "undefined" || data[info.name] === null) {
    if (typeof data[info.name] === "undefined") {
      if (info.canBeUndefined) return undefined
      else throw Error("No " + info.name + "(" + info.type + ") sended")
    }
    if (data[info.name] === null) {
      if (info.canBeNull) return null
      else {
        throw Error(
          "False type with " +
            info.name +
            " - " +
            "null" +
            " instead " +
            info.type,
        )
      }
    }
  }
  if (typeof data[info.name] !== info.type) {
    throw Error(
      "False type with " +
        info.name +
        " - " +
        typeof data[info.name] +
        " instead " +
        info.type,
    )
  }
  if (info?.validate) info?.validate(data[info.name])
  return data[info.name]
}
