import {
  ComplexParseScheme,
  ParseScheme,
  Primitive,
} from "../../abstractions/base/parser"

export const getPartialParseScheme = (source: ParseScheme): ParseScheme => {
  if ((<Primitive>source).type) return { ...source, canBeUndefined: true }
  source = source as ComplexParseScheme
  const result: ParseScheme = {
    primitives: [],
  }
  for (let i = 0; i < source.primitives.length; i++) {
    result.primitives.push({
      ...source.primitives[i],
      canBeUndefined: true,
    })
  }
  if (!source.nested) return result
  result.nested = []
  for (let i = 0; i < source.nested.length; i++) {
    result.nested.push({
      ...(<ComplexParseScheme>getPartialParseScheme(source.nested[i])),
      name: source.nested[i].name,
      canBeUndefined: true,
    })
  }
  return result
}
