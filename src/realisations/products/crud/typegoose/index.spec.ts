/* eslint-env mocha */
import { expect } from "chai"
import { ProductStorage } from "."
import { useBase } from "../../../base-test"

describe("product", () => {
  describe("crud", () => {
    describe("typegoose", () => {
      useBase()
      const productStorage = new ProductStorage()
      const uuid = "test uuid"
      it("create", async () => {
        await productStorage.create({
          uuid,
          name: "name",
          price: 23,
          creationTime: Date.now(),
        })
      })
      it("create exist", async () => {
        try {
          await productStorage.create({
            uuid,
            name: "name",
            price: 23,
            creationTime: Date.now(),
          })
        } catch (e) {
          return
        }
        throw Error("Was created")
      })
      it("read exist", async () => {
        const product = await productStorage.read(uuid)
        expect(product).to.property("uuid", uuid)
      })
      it("update", async () => {
        await productStorage.update(uuid, { name: "name 2" })
        const product = await productStorage.read(uuid)
        expect(product).to.property("name", "name 2")
      })
      it("readMany", async () => {
        await productStorage.readMany()
      })
      it("delete exist", async () => {
        await productStorage.delete(uuid)
        try {
          await productStorage.read(uuid)
        } catch (e) {
          return
        }
        throw Error("Was not deleted")
      })
      it("read unexist", async () => {
        try {
          await productStorage.read(uuid)
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was readed")
      })
      it("update unexist", async () => {
        try {
          await productStorage.update(uuid, {})
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was updated")
      })
      it("delete unexist", async () => {
        try {
          await productStorage.delete(uuid)
        } catch (e) {
          return expect(e).to.have.property("message", "Such was not found")
        }
        throw Error("Was readed")
      })
    })
  })
})
