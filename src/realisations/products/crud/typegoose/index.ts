import { Filter } from "../../../../abstractions/base/crud/filter"
import { ProductInterface } from "../../../../abstractions/products"
import { TypegooseStorageAbstract } from "../../../crud/typegoose"
import { ProductModel } from "./model"

export class ProductStorage extends TypegooseStorageAbstract<ProductInterface> {
  create = async (object: ProductInterface): Promise<void> => {
    await ProductModel.create(object)
  }

  read = async (uuid: string): Promise<ProductInterface> => {
    const result = await ProductModel.findOne({ uuid })
    if (!result) throw Error("Such was not found")
    return result
  }

  update = async (
    uuid: string,
    updateInfo: Partial<ProductInterface>,
  ): Promise<void> => {
    const result = await ProductModel.findOneAndUpdate({ uuid }, updateInfo)
    if (!result) throw Error("Such was not found")
  }

  delete = async (uuid: string): Promise<void> => {
    const result = await ProductModel.findOneAndDelete({ uuid })
    if (!result) throw Error("Such was not found")
  }

  readMany = async (
    filter?: Filter<ProductInterface>,
  ): Promise<ProductInterface[]> => {
    return await ProductModel.find(this.convertFilter(filter))
  }
}
