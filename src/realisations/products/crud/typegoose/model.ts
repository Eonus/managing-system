import { getModelForClass, prop } from "@typegoose/typegoose"
import { ProductInterface } from "../../../../abstractions/products"

class Product implements ProductInterface {
  @prop({ unique: true })
  uuid!: string

  @prop()
  name!: string

  @prop()
  price!: number

  @prop()
  creationTime!: number
}
export const ProductModel = getModelForClass(Product)
