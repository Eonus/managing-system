import { StorageInterface } from "../../../abstractions/base/crud"
import {
  ProductRestApiHandlerInterface,
  ProductRestObject,
} from "../../../abstractions/products/handler"
import { v4 as uuid } from "uuid"
import { ProductInterface } from "../../../abstractions/products"
import { Filter } from "../../../abstractions/base/crud/filter"

export class ProductRestApiHandler implements ProductRestApiHandlerInterface {
  private storage: StorageInterface<ProductInterface>
  constructor(storage: StorageInterface<ProductInterface>) {
    this.storage = storage
  }

  getOne = async (uuid: string): Promise<ProductInterface> => {
    return await this.storage.read(uuid)
  }

  getMany = async (
    filter?: Filter<ProductInterface>,
  ): Promise<ProductInterface[]> => {
    return await this.storage.readMany(filter)
  }

  post = async (object: Omit<ProductRestObject, "uuid">): Promise<string> => {
    const fullObject: ProductInterface = {
      uuid: uuid(),
      ...object,
      creationTime: object.creationTime || Date.now(),
    }
    await this.storage.create(fullObject)
    return fullObject.uuid
  }

  put = async (
    uuid: string,
    object: Partial<ProductRestObject>,
  ): Promise<void> => {
    await this.storage.update(uuid, object)
  }

  delete = async (uuid: string): Promise<void> => {
    await this.storage.delete(uuid)
  }
}
