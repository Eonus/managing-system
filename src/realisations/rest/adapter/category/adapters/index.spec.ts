/* eslint-env mocha */
import axios from "axios"
import { expect } from "chai"
import { RestAdapterTesterInterface } from "../../../../../abstractions/base/rest/adapter/category/index.spec"

export class RestAdapterTesterBase implements RestAdapterTesterInterface {
  protected category: string
  protected url: string
  constructor(url: string, category: string) {
    this.category = category
    this.url = url
  }

  getManySuccess = async (filter?: unknown): Promise<any[]> => {
    const result = await axios.get(this.url + this.category, { params: filter })
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("elements")
    return data.elements
  }

  getManyError = async (
    filter?: unknown,
    errorMessage?: string,
  ): Promise<void> => {
    let result
    if (!filter) result = await axios.get(this.url + "unexisting")
    else result = await axios.get(this.url + this.category, { params: filter })
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("status", "error")
    expect(data).to.have.property("error")
    const error = data.error
    expect(error).to.have.property(
      "message",
      errorMessage || "Such category is not available",
    )
  }

  createOneSuccess = async (object: unknown): Promise<string> => {
    const result = await axios.post(this.url + this.category, object)
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("status", "ok")
    expect(data).to.have.property("objectUuid")
    return data.objectUuid
  }

  updateOneSuccess = async (uuid: string, object: unknown): Promise<void> => {
    const result = await axios.put(
      this.url + this.category + "/" + uuid,
      object,
    )
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("status", "ok")
  }

  updateOneError = async (
    uuid: string,
    object: unknown,
    errorMessage?: string,
  ): Promise<void> => {
    const result = await axios.put(
      this.url + this.category + "/" + uuid,
      object,
    )
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("error")
    const error = data.error
    if (errorMessage) expect(error).to.have.property("message", errorMessage)
  }

  getOneSuccess = async (uuid: string): Promise<unknown> => {
    const result = await axios.get(this.url + this.category + "/" + uuid)
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("element")
    return data.element
  }

  deleteOneSuccess = async (uuid: string): Promise<void> => {
    const result = await axios.delete(this.url + this.category + "/" + uuid)
    expect(result).to.have.property("status", 200)
    const data = result.data
    expect(data).to.have.property("status", "ok")
  }
}
