/* eslint-env mocha */
import { expect } from "chai"
import { useBase } from "../../../../base-test"
import { useExpressServerBase } from "../../../../server/express/index.spec"
import * as moment from "moment"
import { RestAdapterTesterBase } from "./index.spec"

const CATEGORY = "invoice"
const PRODUCT_CATEGORY = "product"
const ORDER_CATEGORY = "order"
const INPUT_DATE_FORMAT = "DD:MM:YYYY"

describe("rest", () => {
  describe(CATEGORY, () => {
    describe("adapter", () => {
      const { PORT } = useBase()
      const { server, URL } = useExpressServerBase(PORT)

      const testerBase = new RestAdapterTesterBase(URL, CATEGORY)
      const productTesterBase = new RestAdapterTesterBase(URL, PRODUCT_CATEGORY)
      const orderTesterBase = new RestAdapterTesterBase(URL, ORDER_CATEGORY)
      let objectUuid: string
      let orderId: string
      let productId: string
      let productObject: {
        name: string
        price: number
        creationTime?: number
      } = {
        name: "name",
        price: 20,
      }
      const orderObject: {
        productId?: string
      } = {
        productId: undefined,
      }
      const object: {
        orderId?: string
      } = {
        orderId: undefined,
      }
      const objectUpdate = {
        discount: 10,
        finalPrice: 18,
      }

      before(async () => {
        await server.initialize()
        await server.connect(PORT)
      })
      after(async () => {
        await server.disconnect()
      })

      it("get many success", async () => {
        await testerBase.getManySuccess()
      })
      it("get many error", async () => {
        await testerBase.getManyError()
      })
      it("create one", async () => {
        productId = await productTesterBase.createOneSuccess(productObject)
        orderObject.productId = productId
        orderId = await orderTesterBase.createOneSuccess(orderObject)
        object.orderId = orderId
        objectUuid = await testerBase.createOneSuccess(object)
      })
      it("get one", async () => {
        const element = await testerBase.getOneSuccess(objectUuid)
        expect(element).to.have.property("finalPrice", 20)
        expect(element).to.have.property("order")
        const order = (<any>element).order
        expect(order).to.have.property("product")
        const product = order.product
        expect(product).to.have.property("price", 20)
      })
      it("update one", async () => {
        await testerBase.updateOneSuccess(objectUuid, objectUpdate)
      })
      it("update one unexist", async () => {
        await testerBase.updateOneError("unexist", {}, "Such was not found")
      })
      it("update one with unexist order", async () => {
        await testerBase.updateOneError(
          objectUuid,
          { orderId: "unexist" },
          "Such was not found",
        )
      })
      it("update one with false discount", async () => {
        await testerBase.updateOneError(
          objectUuid,
          { discount: -1 },
          "Discount out of scope",
        )
      })
      const dateBeforeNow = moment()
        .subtract(1, "day")
        .format(INPUT_DATE_FORMAT)
      const dateAfterNow = moment().add(2, "days").format(INPUT_DATE_FORMAT)
      it("get many with dateFrom", async () => {
        const elements = await testerBase.getManySuccess({
          dateFrom: dateBeforeNow,
        })
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].uuid === objectUuid) return
        }
        throw Error("Object was not finded")
      })
      it("get many with dateFrom unexist", async () => {
        const elements = await testerBase.getManySuccess({
          dateFrom: dateAfterNow,
        })
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].uuid === objectUuid) {
            throw Error("Object was finded")
          }
        }
      })
      it("get many with dateTo", async () => {
        const elements = await testerBase.getManySuccess({
          dateTo: dateAfterNow,
        })
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].uuid === objectUuid) return
        }
        throw Error("Object was not finded")
      })
      it("get many with dateTo unexist", async () => {
        const elements = await testerBase.getManySuccess({
          dateTo: dateBeforeNow,
        })
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].uuid === objectUuid) {
            throw Error("Object was not finded")
          }
        }
      })
      it("get many with both exist", async () => {
        const elements = await testerBase.getManySuccess({
          dateTo: dateAfterNow,
          dateFrom: dateBeforeNow,
        })
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].uuid === objectUuid) return
        }
        throw Error("Object was not finded")
      })
      it("get many with both unexist", async () => {
        const elements = await testerBase.getManySuccess({
          dateTo: dateBeforeNow,
          dateFrom: dateAfterNow,
        })
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].uuid === objectUuid) throw Error("Object was finded")
        }
      })
      it("get many with false format", async () => {
        await testerBase.getManyError(
          { dateFrom: "32.12.2001", dateTo: dateBeforeNow },
          "Parse failed - invalid date",
        )
        await testerBase.getManyError(
          { dateFrom: dateAfterNow, dateTo: "32.12.2001" },
          "Parse failed - invalid date",
        )
      })
      it("delete one", async () => {
        await productTesterBase.deleteOneSuccess(productId)
        await orderTesterBase.deleteOneSuccess(orderId)
        await testerBase.deleteOneSuccess(objectUuid)
      })
      it("create invoice with discount", async () => {
        productObject = {
          creationTime: moment().subtract(31, "days").valueOf(),
          name: "name 2",
          price: 20,
        }
        productId = await productTesterBase.createOneSuccess(productObject)
        orderObject.productId = productId
        orderId = await orderTesterBase.createOneSuccess(orderObject)
        object.orderId = orderId
        objectUuid = await testerBase.createOneSuccess(object)
      })
      it("get one with discount", async () => {
        const element = await testerBase.getOneSuccess(objectUuid)
        expect(element).to.have.property("finalPrice", 16)
      })
      it("update one with discount", async () => {
        const creationTime = moment().valueOf()
        await productTesterBase.updateOneSuccess(productId, { creationTime })
        await orderTesterBase.updateOneSuccess(orderId, { productId })
        await testerBase.updateOneSuccess(objectUuid, { orderId })
      })
      it("get one without discount", async () => {
        const element = await testerBase.getOneSuccess(objectUuid)
        expect(element).to.have.property("finalPrice", 20)
      })
      it("update discount", async () => {
        await testerBase.updateOneSuccess(objectUuid, { discount: 50 })
        const element = await testerBase.getOneSuccess(objectUuid)
        expect(element).to.have.property("discount", 50)
        expect(element).to.have.property("finalPrice", 10)
      })
      it("clear product, order and invoice with discount", async () => {
        await productTesterBase.deleteOneSuccess(productId)
        await orderTesterBase.deleteOneSuccess(orderId)
        await testerBase.deleteOneSuccess(objectUuid)
      })
    })
  })
})
