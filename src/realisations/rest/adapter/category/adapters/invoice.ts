import * as moment from "moment"
import { Moment } from "moment"
import { RestApiCategoryAdapterAbstract } from "."
import { Filter } from "../../../../../abstractions/base/crud/filter"
import {
  ParseScheme,
  PrimitiveType,
} from "../../../../../abstractions/base/parser"
import { InvoiceInterface } from "../../../../../abstractions/invoices"
import {
  InvoiceRestApiHandlerInterface,
  InvoiceRestObject,
} from "../../../../../abstractions/invoices/handler"
import { parseData } from "../../../../parser"
import { getPartialParseScheme } from "../../../../parser/scheme"

const INPUT_DATE_FORMAT = "DD:MM:YYYY"

export class RestApiInvoiceAdapter extends RestApiCategoryAdapterAbstract<
  InvoiceRestObject,
  InvoiceInterface,
  InvoiceRestApiHandlerInterface
> {
  protected handler!: InvoiceRestApiHandlerInterface
  protected scheme: ParseScheme = {
    primitives: [
      {
        name: "orderId",
        type: PrimitiveType.String,
      },
      {
        name: "discount",
        type: PrimitiveType.Number,
        canBeUndefined: true,
        validate: (value: number): void => {
          if (value > 100 || value < 0) throw Error("Discount out of scope")
        },
      },
    ],
  }

  protected getAllScheme: ParseScheme = {
    primitives: [
      {
        name: "dateFrom",
        type: PrimitiveType.String,
        canBeUndefined: true,
      },
      {
        name: "dateTo",
        type: PrimitiveType.String,
        canBeUndefined: true,
      },
    ],
  }

  protected put = async (data: unknown, uuid?: string): Promise<any> => {
    if (!uuid) throw Error("No uuid sended")
    const invoice = parseData<Partial<InvoiceRestObject>>(
      getPartialParseScheme(this.scheme),
      data,
    )
    await this.handler.put(uuid, invoice)
  }

  protected getMany = async (data: unknown): Promise<any> => {
    const filterTemplate = <{ dateFrom?: string; dateTo?: string }>(
      parseData(this.getAllScheme, data)
    )
    let dateTo: Moment | null = null
    let dateFrom: Moment | null = null
    if (filterTemplate.dateFrom) {
      dateFrom = moment(filterTemplate.dateFrom, INPUT_DATE_FORMAT)
      if (!dateFrom.isValid()) throw Error("Parse failed - invalid date")
      dateFrom = dateFrom.startOf("day")
    }
    if (filterTemplate.dateTo) {
      dateTo = moment(filterTemplate.dateTo, INPUT_DATE_FORMAT)
      if (!dateTo.isValid()) throw Error("Parse failed - invalid date")
      dateTo = dateTo.endOf("day")
    }
    const filter: Filter<InvoiceRestObject> = {}
    if (dateFrom) {
      if (!filter.creationTime) {
        filter.creationTime = { $more: dateFrom.valueOf() }
      } else filter.creationTime.$more = dateFrom.valueOf()
    }
    if (dateTo) {
      if (!filter.creationTime) {
        filter.creationTime = { $less: dateTo.valueOf() }
      } else filter.creationTime.$less = dateTo.valueOf()
    }
    return { elements: await this.handler.getMany(filter) }
  }
}
