/* eslint-env mocha */
import { useBase } from "../../../../base-test"
import { useExpressServerBase } from "../../../../server/express/index.spec"
import { expect } from "chai"
import { RestAdapterTesterBase } from "./index.spec"

const CATEGORY = "order"
const PRODUCT_CATEGORY = "product"

describe("rest", () => {
  describe(CATEGORY, () => {
    describe("adapter", () => {
      const { PORT } = useBase()
      const { server, URL } = useExpressServerBase(PORT)

      const testerBase = new RestAdapterTesterBase(URL, CATEGORY)
      const productTesterBase = new RestAdapterTesterBase(URL, PRODUCT_CATEGORY)
      let objectUuid: string
      let productId: string
      const productObject = {
        name: "name",
        price: 20,
      }
      const object: {
        productId?: string
      } = {
        productId: undefined,
      }
      const objectUpdate = {
        status: "processed",
      }

      before(async () => {
        await server.initialize()
        await server.connect(PORT)
      })
      after(async () => {
        await server.disconnect()
      })

      it("get many success", async () => {
        await testerBase.getManySuccess()
      })
      it("get many error", async () => {
        await testerBase.getManyError()
      })
      it("create one", async () => {
        productId = await productTesterBase.createOneSuccess(productObject)
        object.productId = productId
        objectUuid = await testerBase.createOneSuccess(object)
      })
      it("get one", async () => {
        const element = await testerBase.getOneSuccess(objectUuid)
        expect(element).to.have.property("status", "created")
      })
      it("update one", async () => {
        await testerBase.updateOneSuccess(objectUuid, objectUpdate)
      })
      it("update one with false status", async () => {
        await testerBase.updateOneError(
          objectUuid,
          { status: "status" },
          "Parse status failed",
        )
      })
      it("update one with unexist", async () => {
        await testerBase.updateOneError("unexist", {}, "Such was not found")
      })
      it("update one with unexist product", async () => {
        await testerBase.updateOneError(
          objectUuid,
          { productId: "unexist" },
          "Such was not found",
        )
      })
      it("delete one", async () => {
        await productTesterBase.deleteOneSuccess(productId)
        await testerBase.deleteOneSuccess(objectUuid)
      })
    })
  })
})
