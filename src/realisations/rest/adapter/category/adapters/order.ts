import { RestApiCategoryAdapterAbstract } from "."
import {
  ParseScheme,
  PrimitiveType,
} from "../../../../../abstractions/base/parser"
import { OrderInterface, OrderStatus } from "../../../../../abstractions/orders"
import {
  OrderRestApiHandlerInterface,
  OrderRestObject,
} from "../../../../../abstractions/orders/handler"
import { parseData } from "../../../../parser"
import { getPartialParseScheme } from "../../../../parser/scheme"

export class RestApiOrderAdapter extends RestApiCategoryAdapterAbstract<
  OrderRestObject,
  OrderInterface,
  OrderRestApiHandlerInterface
> {
  protected handler!: OrderRestApiHandlerInterface
  protected scheme: ParseScheme = {
    primitives: [
      {
        name: "productId",
        type: PrimitiveType.String,
      },
      {
        name: "status",
        type: PrimitiveType.String,
        canBeUndefined: true,
        validate: (value: string): void => {
          const index = Object.values(OrderStatus).indexOf(value as any)
          if (index === -1) throw Error("Parse status failed")
        },
      },
    ],
  }

  protected put = async (data: unknown, uuid?: string): Promise<any> => {
    if (!uuid) throw Error("No uuid sended")
    const order = parseData<Partial<OrderRestObject>>(
      getPartialParseScheme(this.scheme),
      data,
    )
    await this.handler.put(uuid, order)
  }
}
