/* eslint-env mocha */
import { useBase } from "../../../../base-test"
import { useExpressServerBase } from "../../../../server/express/index.spec"
import { expect } from "chai"
import { RestAdapterTesterBase } from "./index.spec"

const CATEGORY = "product"

describe("rest", () => {
  describe(CATEGORY, () => {
    describe("adapter", () => {
      const { PORT } = useBase()
      const { server, URL } = useExpressServerBase(PORT)

      const testerBase = new RestAdapterTesterBase(URL, CATEGORY)
      const object = {
        name: "name",
        price: 20,
      }
      const objectUpdate = {
        name: "name 2",
      }
      let objectUuid: string

      before(async () => {
        await server.initialize()
        await server.connect(PORT)
      })
      after(async () => {
        await server.disconnect()
      })

      it("get many success", async () => {
        await testerBase.getManySuccess()
      })
      it("get many error", async () => {
        await testerBase.getManyError()
      })
      it("create one", async () => {
        objectUuid = await testerBase.createOneSuccess(object)
      })
      it("update one", async () => {
        await testerBase.updateOneSuccess(objectUuid, objectUpdate)
      })
      it("update one unexist", async () => {
        await testerBase.updateOneError("unexist", {}, "Such was not found")
      })
      it("get one", async () => {
        const element = await testerBase.getOneSuccess(objectUuid)
        expect(element).to.have.property("name", "name 2")
      })
      it("delete one", async () => {
        await testerBase.deleteOneSuccess(objectUuid)
      })
    })
  })
})
