import { RestApiCategoryAdapterAbstract } from "."
import { parseData } from "../../../../parser"
import { getPartialParseScheme } from "../../../../parser/scheme"
import {
  ProductRestApiHandlerInterface,
  ProductRestObject,
} from "../../../../../abstractions/products/handler"
import {
  ParseScheme,
  PrimitiveType,
} from "../../../../../abstractions/base/parser"
import { ProductInterface } from "../../../../../abstractions/products"

export class RestApiProductAdapter extends RestApiCategoryAdapterAbstract<
  ProductRestObject,
  ProductInterface,
  ProductRestApiHandlerInterface
> {
  protected handler!: ProductRestApiHandlerInterface
  protected scheme: ParseScheme = {
    primitives: [
      {
        name: "name",
        type: PrimitiveType.String,
      },
      {
        name: "price",
        type: PrimitiveType.Number,
      },
      {
        name: "creationTime",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
    ],
  }

  protected put = async (data: unknown, uuid?: string): Promise<any> => {
    if (!uuid) throw Error("No uuid sended")
    const product = parseData<Partial<ProductRestObject>>(
      getPartialParseScheme(this.scheme),
      data,
    )
    await this.handler.put(uuid, product)
  }
}
