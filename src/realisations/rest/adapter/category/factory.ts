import { RestApiCategoryAdapterInterface } from "../../../../abstractions/base/rest/adapter/category"
import { RestApiCategoryAdapterFactoryInterface } from "../../../../abstractions/base/rest/adapter/category/factory"
import { InvoiceRestApiHandlerInterface } from "../../../../abstractions/invoices/handler"
import { OrderRestApiHandlerInterface } from "../../../../abstractions/orders/handler"
import { ProductRestApiHandlerInterface } from "../../../../abstractions/products/handler"
import { RestApiInvoiceAdapter } from "./adapters/invoice"
import { RestApiOrderAdapter } from "./adapters/order"
import { RestApiProductAdapter } from "./adapters/product"

export class RestApiCategoryAdapterFactory
  implements RestApiCategoryAdapterFactoryInterface {
  private productAdapter?: RestApiCategoryAdapterInterface
  private productHandler: ProductRestApiHandlerInterface

  private orderAdapter?: RestApiCategoryAdapterInterface
  private orderHandler: OrderRestApiHandlerInterface

  private invoiceAdapter?: RestApiCategoryAdapterInterface
  private invoiceHandler: InvoiceRestApiHandlerInterface
  constructor(
    productHandler: ProductRestApiHandlerInterface,
    orderHandler: OrderRestApiHandlerInterface,
    invoiceHandler: InvoiceRestApiHandlerInterface,
  ) {
    this.productHandler = productHandler
    this.orderHandler = orderHandler
    this.invoiceHandler = invoiceHandler
  }

  getAdapter = (category: string): RestApiCategoryAdapterInterface => {
    if (category === "product") return this.getProductAdapter()
    if (category === "order") return this.getOrderAdapter()
    if (category === "invoice") return this.getInvoiceAdapter()
    throw Error("Such category is not available")
  }

  private getProductAdapter = () => {
    if (!this.productAdapter) {
      this.productAdapter = new RestApiProductAdapter(this.productHandler)
    }
    return this.productAdapter
  }

  private getOrderAdapter = () => {
    if (!this.orderAdapter) {
      this.orderAdapter = new RestApiOrderAdapter(this.orderHandler)
    }
    return this.orderAdapter
  }

  private getInvoiceAdapter = () => {
    if (!this.invoiceAdapter) {
      this.invoiceAdapter = new RestApiInvoiceAdapter(this.invoiceHandler)
    }
    return this.invoiceAdapter
  }
}
