import { RestApiMethod } from "../../../../abstractions/base/rest/adapter/category"
import { RestApiCategoryAdapterFactoryInterface } from "../../../../abstractions/base/rest/adapter/category/factory"
import { RestApiServerAdaperInterface } from "../../../../abstractions/base/rest/adapter/server"

enum ResponseStatus {
  Success = "ok",
  Error = "error",
}

type ResponseBase = {
  uuid: string
  status: ResponseStatus
}

type ErrorResponse = ResponseBase & {
  error: {
    message: string
  }
}

type SuccessResponse = ResponseBase & {
  [key: string]: any
}

type Response = SuccessResponse | ErrorResponse

abstract class RestApiServerAdaperAbstract
  implements RestApiServerAdaperInterface {
  abstract count: (
    category: string,
    method: RestApiMethod,
    data: any
  ) => Promise<Response>

  // eslint-disable-next-line node/handle-callback-err
  protected getError = (error: Error, uuid: string): ErrorResponse => {
    return {
      status: ResponseStatus.Error,
      uuid,
      error: {
        message: error.message,
      },
    }
  }

  protected getSuccess = (data: any, uuid: string): SuccessResponse => {
    try {
      data.status = ResponseStatus.Success
    } catch (e) {
      data = { status: ResponseStatus.Success }
    }
    data.uuid = uuid
    return data
  }

  abstract isCategoryExist: (category: string) => boolean
}

export class RestApiServerAdaper extends RestApiServerAdaperAbstract {
  private adapters: RestApiCategoryAdapterFactoryInterface
  constructor(adapters: RestApiCategoryAdapterFactoryInterface) {
    super()
    this.adapters = adapters
  }

  count = async (
    category: string,
    method: RestApiMethod,
    data: unknown,
    uuid?: string,
  ): Promise<Response> => {
    const requestUuid = "request uuid"
    try {
      const adapter = this.adapters.getAdapter(category)
      return this.getSuccess(
        await adapter.call(method, data, uuid),
        requestUuid,
      )
    } catch (e) {
      return this.getError(e, requestUuid)
    }
  }

  isCategoryExist = (category: string): boolean => {
    try {
      this.adapters.getAdapter(category)
    } catch (e) {
      return false
    }
    return true
  }
}
