import { ProductStorage } from "../../products/crud/typegoose"
import { ProductRestApiHandler } from "../../products/handler"
import { RestApiCategoryAdapterFactory } from "../../rest/adapter/category/factory"
import { RestApiServerAdaper } from "../../rest/adapter/server"
import { RestExpressServer } from "./rest"
import { OrderRestApiHandler } from "../../orders/handler"
import { OrderStorage } from "../../orders/crud/typegoose"
import { ServerInterface } from "../../../abstractions/base/server"
import { InvoiceStorage } from "../../invoices/crud/typegoose"
import { InvoiceRestApiHandler } from "../../invoices/handler"

export const useExpressServerBase = (
  port: number,
): { URL: string; server: ServerInterface } => {
  const PORT = port
  const URL = "http://localhost:" + PORT + "/"

  const productStorage = new ProductStorage()
  const productHandler = new ProductRestApiHandler(productStorage)

  const orderStorage = new OrderStorage()
  const orderHandler = new OrderRestApiHandler(orderStorage, productStorage)

  const invoiceStorage = new InvoiceStorage()
  const invoiceHandler = new InvoiceRestApiHandler(
    invoiceStorage,
    productStorage,
    orderStorage,
  )

  const server = new RestExpressServer(
    new RestApiServerAdaper(
      new RestApiCategoryAdapterFactory(
        productHandler,
        orderHandler,
        invoiceHandler,
      ),
    ),
  )
  return {
    URL,
    server,
  }
}
