import { ServerInterface } from "../../../abstractions/base/server"
import * as express from "express"
import { Server } from "http"
const bodyParser = require("body-parser")

export abstract class ExpressServerAbstract implements ServerInterface {
  protected app: express.Application
  protected server?: Server
  constructor() {
    this.app = express()
    this.app.use(bodyParser.json())
  }

  initialize = async (): Promise<void> => {
    await this.initListeners()
  }

  protected abstract initListeners: () => Promise<void>

  connect = async (port: number): Promise<void> => {
    this.server = this.app.listen(port)
  }

  disconnect = async (): Promise<void> => {
    if (this.server) await this.server.close()
  }
}
