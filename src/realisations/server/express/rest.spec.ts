/* eslint-env mocha */
import { useBase } from "../../base-test"
import { useExpressServerBase } from "./index.spec"

describe("server", () => {
  describe("express", () => {
    describe("rest", () => {
      const { PORT } = useBase()
      const { server } = useExpressServerBase(PORT)
      it("initialize", async () => {
        await server.initialize()
      })
      it("connect to " + PORT, async () => {
        await server.connect(PORT)
      })
      it("disconnect", async () => {
        await server.disconnect()
      })
    })
  })
})
