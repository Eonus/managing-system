import { ExpressServerAbstract } from "."
import { RestApiMethod } from "../../../abstractions/base/rest/adapter/category"
import { RestApiServerAdaperInterface } from "../../../abstractions/base/rest/adapter/server"

export class RestExpressServer extends ExpressServerAbstract {
  private adapter: RestApiServerAdaperInterface
  constructor(adapter: RestApiServerAdaperInterface) {
    super()
    this.adapter = adapter
  }

  protected initListeners = async (): Promise<void> => {
    this.app.get("/:category/:uuid", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RestApiMethod.Get,
          req.query,
          req.params.uuid,
        ),
      )
    })
    this.app.get("/:category", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RestApiMethod.GetAll,
          req.query,
        ),
      )
    })
    this.app.post("/:category", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RestApiMethod.Post,
          req.body,
        ),
      )
    })
    this.app.put("/:category/:uuid", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RestApiMethod.Update,
          req.body,
          req.params.uuid,
        ),
      )
    })
    this.app.delete("/:category/:uuid", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RestApiMethod.Delete,
          req.body,
          req.params.uuid,
        ),
      )
    })
  }
}
